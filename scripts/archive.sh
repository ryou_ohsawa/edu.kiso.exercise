#!/bin/bash -x

API_BASE="https://api.bitbucket.org/2.0/repositories/"
API_ENDPOINT="${API_BASE}/ryou_ohsawa/edu.kiso.exercise/downloads"

DATE_TAG="$(date +%Y%m%d)"
BRANCH_TAG="$(git rev-parse --abbrev-ref HEAD | sed 's#.*/##')"
COMMIT_TAG="${BRANCH_TAG}-$(git rev-parse HEAD | cut -c 1-8)"
FILE_SUFFIX="${COMMIT_TAG}"

ZIPFILE="documents_${FILE_SUFFIX}.zip"
CONTENTS="jisshu_note.pdf resource"
zip -r ${ZIPFILE} ${CONTENTS}
