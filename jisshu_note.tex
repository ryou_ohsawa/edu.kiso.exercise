\documentclass[11pt,a4paper,dvipdfmx,uplatex]{jsarticle}
\usepackage{note}
\usepackage{verbatimbox}
\usepackage{hmsdms}
\graphicspath{{figs/}}

\title{基礎天文学観測 --- CMOS 撮像と画像解析}
\author{大澤亮}
\date{\today}

\hypersetup{
  colorlinks=true,
  anchorcolor=black,
  citecolor=[rgb]{0.2,0.2,0.4},
  filecolor=gray,
  linkcolor=[rgb]{0.4,0.2,0.2},
  menucolor=[rgb]{0.0,0.0,0.0},
  pagecolor=[rgb]{0.2,0.2,0.2},
  urlcolor=[rgb]{0.3,0.3,0.8},
  pdftitle={\thetitle},
  pdfsubject={2020 年 基礎天文学観測 補足資料},
  pdfauthor={大澤 亮},
  pdfkeywords={東京大学, 基礎天文学観測},
  pdfproducer={大澤 亮},
  pdfcreator={\LaTeX with hyperref package},
  pdfpagelabels=true,
  pdfstartpage=1,
}


\begin{document}
\maketitle
% \thispagestyle{empty}

\section{Tomo-e Gozen について}
\label{sec:tomoe}

Tomo-e Gozen は東京大学理学系研究科附属天文学教育研究センター木曽観測所 105\,cm シュミット望遠鏡用に開発されたモザイク CMOS カメラである. カメラ全体は 4 つのモジュールから構成されている. 受光面に向かって第一象限 (北西) が Q1, 第二象限が Q2, 第三象限が Q3, 第四象限が Q4 と呼ばれる. それぞれのモジュールには計 21 枚のイメージセンサが搭載されており, 各センサの視野は $39.7'{\times}22.4'$ である. 84 枚のイメージセンサでおよそ $20.8$ 平方度の視野を一度に撮影することができる.

搭載されている CMOS センサはローリングシャッタという読み出し形式を採用している. 機械的なシャッタを持たない携帯電話や廉価なデジタルカメラで採用されている手法である. ローリングシャッタではセンサ内での時間の同一性が失われる代わりに, データの読み出しにかかる時間のロスを抑えることができる. Tomo-e Gozen では最速で 0.5 秒間隔でデータを読みだすことができるが, 読み出しにかかる時間のロスは画素 (ピクセル) あたり 0.1\,ms 程度である. 短時間露光で連続的にデータを読みだしても, 露光時間のロスがほとんどないため動画的な観測が可能である.

Tomo-e Gozen は広い領域を一晩のあいだに何度も観測することで, 超新星ショックブレイクアウトなどのタイムスケールの短い突発現象や高速移動天体を検出することを目的としている. 通常観測時はできる限り光をセンサに入れるため, 光学フィルタを付けずに観測をしている. 今回の実習では色等級図を作ることが必要になるため特別にフィルタを搭載して観測を実施する.

\begin{figure}[ptb]
  \centering
  \hspace{\stretch{2}}%
  \includegraphics[height=.45\linewidth]{tomoe_filter_202010.pdf}
  \hspace{\stretch{1}}%
  \includegraphics[height=.38\linewidth]{tomoe_q1_202010.jpg}
  \hspace{\stretch{1}}%
  \caption{Tomo-e Gozen Q1 に搭載されているフィルタの配置 (2020.10 版). 写真 (右図) はフィルタによって反射された光を見ている. $i'$ バンドフィルタと H$\alpha$ 狭帯域フィルタは可視光の大半を通さないので鏡のように見える. $g'$ バンドフィルタは赤い光を通さないためオレンジ色に見えている. $r'$ バンドフィルタは反対に青い光を通さないので青っぽく輝いて見える.}
  \label{fig:q1:filter}
\end{figure}

一般的な天文用カメラではフィルタ交換機構を用いてフィルタを切り替えて, 視野全体が同じフィルタで観測できるようになっている. Tomo-e Gozen はカメラが大きすぎるため, 視野全体を覆うようなフィルタを制作できていない. またフィルタ交換機構も存在していない. そこで各検出器の前に付いている窓を光学フィルタに交換することで光学フィルタを搭載した. Figure~\ref{fig:q1:filter} に各フィルタの配置と写真を掲示した. 異なる検出器に異なる光学フィルタが割り当てられている. 異なる光学フィルタで観測をおこなうためには, 視野を動かして天体を目的のフィルタに導入する必要がある.

\begin{figure}[tb]
  \centering
  \includegraphics[width=\linewidth]{tomoe_qe.pdf}
  \caption{検出器の波長感度特性とフィルタの透過率.}
  \label{fig:tomoe:Qe}
\end{figure}

今回使用するフィルタは $g'$ および $i'$ バンドである. フィルタは Astrodon というメーカで作られたものであり, 基本的には Sloan Digital Sky Survey というプロジェクトで使用されているフィルタの感度特性にあわせて設計されている. Figure~\ref{fig:tomoe:Qe} に CMOS センサの波長感度特性とフィルタの透過率を示した. $g'$, $i'$ バンドはそれぞれ $475\,$nm 付近と $775\,$nm 付近の光を選択的に透過する. センサの波長感度特性が一定ではないため, 同じ積分時間でも $i'$ バンドの方が検出限界が浅くなる.


\clearpage
\section{観測のてびき}
\label{sec:observation}

観測は木曽観測所本館の観測室から実施する. 本実習で観測する天体は銀河団 Abell 2634 である. ICRS での座標は以下の通り (via \href{http://simbad.u-strasbg.fr/simbad}{\ttfamily Simbad} database).

\begin{quote}
  {\bfseries Abell 2634} (\hms{23}{38}{18.4}, \dms{+27}{01}{37})
\end{quote}

なお, この座標にそのまま望遠鏡を向けても観測はできない. 今回使用する $g'$, $i'$ バンドフィルタは望遠鏡の視野中心からはかなり離れた位置に付いているため, 銀河団の中心が適切な位置に導入されるように望遠鏡をシフトさせる必要がある.

\begin{quote}
  Tomo-e Gozen Field Checker\\
  \href{http://www.ioa.s.u-tokyo.ac.jp/tomoe/aladin/}
  {\ttfamily http://www.ioa.s.u-tokyo.ac.jp/tomoe/aladin/}
\end{quote}

\begin{figure}[b]
  \centering
  \includegraphics[width=\linewidth]{survey_pattern_abell2634.pdf}
  \caption{観測計画の例. センサは横に $25.0'$ 離れて配置されている. $2{\times}3$ のディザリング観測を実施することでおよそ $3.4^\circ{\times}1.0^\circ$ の領域を観測できる. $g'$, $i'$ バンドそれぞれで白枠に示された領域を観測する.}
  \label{fig:tomoe:dithering}
\end{figure}

Tomo-e Gozen Field Checker は天体を目的の検出器に導入するための補助ツールである. \fancybox{Mov} を選択してから天体を目的の位置にドラッグすることで望遠鏡中心を向けるべき座標が計算される. ディザリングのパターンを考えて初期位置を考える必要がある. 参考として Figure~\ref{fig:tomoe:dithering} に $3.4^\circ{\times}1.0^\circ$ の領域をカバーするための初期位置とディザリングのパターンを示した.

観測は命令を記述したレシピファイルをシステムに登録することで実行する. Figure~\ref{fig:recipe:gband} に $g'$ バンドで領域を 1 周分観測を実行するためのレシピを記載する (1 回の露出を 2 分間とした).

\begin{figure}
  \centering
  \inputrecipe{resource/abell2634_g.recipe}{abell2634\_g.recipe}
  \caption{$g'$ バンドで Abell 2634 を観測するためのレシピファイル.}
  \label{fig:recipe:gband}
\end{figure}
% \inputrecipe{resource/abell2634_i.recipe}{abell2634\_i.recipe}

レシピは \texttt{YAML} という書式で記述されており, \texttt{Operations} 以下に列挙されている内容が順番に実行される. 個々の命令の意味は以下の通り.

\begin{description}[leftmargin=2zw,topsep=0zw]
\item[\texttt{Assert: assertion\_items,...}]\ \\
  望遠鏡や装置の現在のステータスをチェックして, 条件を満たしていなければエラー停止させる.
\item[\texttt{Pointing: [ra, dec]}]\ \\
  望遠鏡の視野中心を (\texttt{ra}, \texttt{dec}) に向ける.
\item[\texttt{Wait: waiting\_items,...}]\ \\
  動作が完了するまでレシピの実行を停止させる. ここでは望遠鏡のポインティングが完了するのを待っている. 決められた時間内に動作が完了しなかった場合には異常とみなしてエラー停止させる.
\item[\texttt{MirrorCover: \{open|close\}}]\ \\
  望遠鏡のミラーカバーを操作する. 引数として \texttt{open} あるいは \texttt{close} をとる.
\item[\texttt{ResetParameter}]\ \\
  カメラの設定をデフォルト値に直す. フォーカスの値だけは変えないので注意.
\item[\texttt{SetParameter: ...}]\ \\
  カメラの設定を変更する. 最初の \texttt{SetParameter} ではカメラの出力ゲインを \texttt{high} に, 1 フレームあたりの露出秒数を 1.0 秒に, \texttt{Exposure} あたりのフレーム取得数を 120 枚に設定している. 次の \texttt{SetParameter} ではデータを取得する検出器を実習で使用する検出器のみに限定している. 引数と検出器の対応関係を Figure~\ref{fig:tomoe:detectors} に示した.
\item[\texttt{SetPipeline: ...}]\ \\
  データの処理を行うための解析システムに処理方法 (解析パイプライン) を指定する. ここでは \texttt{wcs}, \texttt{jisshu} が指定されている. \texttt{wcs} パイプラインでは星の位置とカタログを参照して, それぞれの検出器で撮影した画像に正しい座標系を設定する. 曇ってしまったときなど, 星が十分に写っていない場合には失敗することもある. \texttt{wcs} パイプラインでエラーとなった場合, そのデータはサイエンスに使えないデータだと判断して解析を中断する. \texttt{jisshu} パイプラインでは動画データを時間軸方向に足し合わせて\footnote{足し合わせでは ``最大値を除いた平均'' 処理をおこなっている.}観測実習で使用するための画像を作成する.
\item[\texttt{Exposure: object\_name}]\ \\
  データを取得する. 引数として天体名をとる.
\item[\texttt{Dithering: [delta\_ra, delta\_dec]}]\ \\
  望遠鏡を現在の位置から相対的に微動させる. 引数は RA, Dec 方向の秒角単位で与える. ただし, 望遠鏡は RA, Dec の座標に沿って移動するため, 特に高緯度領域で大きく移動させると, 思っていた位置とは違う位置に移動することもあるので注意すること.
\end{description}

\begin{figure}
  \centering
  \includegraphics[width=0.48\linewidth]{figs/tomoe_det_id.pdf}
  \hspace*{\stretch{1}}
  \includegraphics[width=0.48\linewidth]{figs/tomoe_frp.pdf}
  \caption{検出器の名称と対応する FRP 番号. 検出器 \texttt{111}, \texttt{112} は Q1 の \texttt{FRP05} というプロセスが管轄している. 特定の検出器のみでデータを取得する場合には \texttt{SetParameter} で FRP 番号を指定する.}
  \label{fig:tomoe:detectors}
\end{figure}


\clearpage
\section{整約{\textbullet}解析のてびき}
\label{sec:reduction}

観測で得た生データには観測装置や望遠鏡のさまざまな特性が反映されており, 観測対象を忠実に写し取ったものにはなっていない. 装置や望遠鏡の特性を生データから取り除くことを整約という.

データ整約や解析に必要なデータは以下のページからダウンロードできる.

\begin{quote}
  基礎天文学観測「CMOS 撮像と画像解析」補助ページ\\
  \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/jisshu/}
  {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/jisshu/}
\end{quote}

観測したデータは Tomo-e Gozen Archive というウェブページから取得することができる.

\begin{quote}
  Tomo-e Gozen Archive\\
  \href{http://tomoearv-master.kiso.ioa.s.u-tokyo.ac.jp/archive/}
  {\ttfamily http://tomoearv-master.kiso.ioa.s.u-tokyo.ac.jp/archive/}
\end{quote}

Tomo-e Gozen のデータ名は以下のようなフォーマットで定められている.

\begin{quote}
  {\ttfamily \textbf{a\,}TMQ\textbf{\,q\,}\textbf{yyyymmdd\,}\textbf{NNNNNNNN\,}\textbf{DD}.fits}
  \small
  \begin{itemize}[leftmargin=6em]
  \item[\ttfamily\bfseries a] プレフィックス, データの種別を表す (\texttt{d}: ダーク, \texttt{f}: フラット, \texttt{x}: 実習用観測データ)
  \item[\ttfamily\bfseries q] カメラモジュール番号 (\texttt{1..4})
  \item[\ttfamily\bfseries yyyymmdd] 日付 (UTC)
  \item[\ttfamily\bfseries NNNNNNNN] 露出番号 (通し番号)
  \item[\ttfamily\bfseries DD] 検出器番号
  \end{itemize}
\end{quote}

ブラウザからアクセスして然るべきリンクをクリックして手元にデータを取得する. ファイルの総数が多い場合は別途まとめてダウンロードする方法を用意する.


\setcounter{subsection}{-1}
\subsection{画像演算スクリプトのつかいかた}
\label{sec:reduction:imarith}
実習では簡単な画像同士の演算を実行するスクリプト \texttt{imarith} を使用する. これは画像と数値, あるいは 2 枚の画像の四則演算を実行して, 結果を画像として保存するためのスクリプトである. 実習では差分 (\texttt{-}) と除算 (\texttt{/}) を使用する. \texttt{imarith} のヘルプメッセージを以下に貼り付ける.

\begin{shell}
usage: imarith [-h] [-f] lhs op rhs out

Arithmetic operations on images.

positional arguments:
  lhs         left operand (image)
  op          operator {+, -, *, x, /}
  rhs         right operand (image or number)
  out         output fits file

optional arguments:
  -h, --help  show this help message and exit
  -f          overwrite a fits image
\end{shell}


\subsection{バイアスの補正}
\label{sec:reduction:bias}

Tomo-e Gozen のデータは予めバイアス処理をした状態で出力されるので省略する.


\subsection{暗電流の補正}
\label{sec:reduction:dark}

暗電流を補正するためのデータを Tomo-e Gozen Archive から取得する. 観測ログを参照しながら対応する積分時間の暗電流画像をダウンロードして観測画像から減算する. 暗電流の値は検出器ごとにことなるので検出器番号を間違えないこと.

\begin{shell}
imarith xTMQ1NNNNNNNNDD.fits - dTMQ1NNNNNNNNDD.fits dxTMQ1NNNNNNNNDD.fits
\end{shell}


\subsection{フラットの補正}
\label{sec:reduction:dark}

フラットを補正するためのデータを Tomo-e Gozen Archive から取得する. 観測ログを参照しながら対応する観測画像を割る. フラットパターンは検出器ごとにことなるので検出器番号を間違えないこと.

\begin{shell}
imarith dxTMQ1NNNNNNNNDD.fits - fTMQ1NNNNNNNNDD.fits fdxTMQ1NNNNNNNNDD.fits
\end{shell}


\subsection{アストロメトリ}
\label{sec:reduction:wcs}

資料に示した通りのレシピで観測していた場合, アストロメトリに成功したものだけが Tomo-e Gozen Archive に現れる. 曇っていたりトラッキングに失敗しているなどしてアストロメトリができないデータは排除されている.


\subsection{等級原点の較正}
\label{sec:reduction:zeromag}

実習では複数の視野で撮影した画像をつなぎ合わせるモザイク処理をおこなう. 異なる時間で撮影した画像は大気の透過率が異なる. 透過率の変動を補正するための作業が等級原点の較正である. ここ SDSS で提供されたの明るい星のカタログを参照して等級原点が 25 等級になるようにカウント値をスケールする. ここでは \texttt{zeromag} というスクリプトを使用する.

\begin{shell}
usage: zeromag [-h] [--radius size] [--box-width size] [--filter-width size]
               [--saturation flux] [-f]
               catalog band fits new

Calculate the magnitude zero point and generate a new fits image whose magzpt
is adjusted to 25.0.

positional arguments:
  catalog               SDSS12 catalog
  band                  photometric band name
  fits                  input fits file
  new                   output fits file

optional arguments:
  -h, --help           show this help message and exit
  --radius size        aperture radius photometry
  --box-width size     background estimation box size
  --filter-width size  background estimation box size
  --saturation flux    saturation limit
  -f, --overwrite      overwrite existing files
\end{shell}

\texttt{zeromag} は Abell 2634 領域の測光カタログ, カタログに記載された星の等級値を示すカラム名, 入力画像名, 出力画像名を引数にとる. 出力される画像では背景光が差し引かれ, 等級原点が 25 等になるようにカウント値がスケールされている. 実行結果のサンプルを以下に貼り付ける.

\begin{shell}
$ ./zeromag abell2634_sdss12.tbl gmag input.fits output.fits
zeromag summary of "input.fits".
  photometry radius : 5.0
  number of stars   : 19
  background level  : 69.052
  background stddev : 2.638
  zeromag mean      : 22.690
  zeromag median    : 22.603
  zeromag stddev    : 0.405
  zeromag error     : 0.093
\end{shell}

ここでは \texttt{abell2634\_sdss12.tbl} というカタログ (テキストファイル) の \texttt{gmag} というカラムを使用して \texttt{input.fits} という画像ファイルを処理した. このファイルの等級原点はおよそ 22.7 等級だった. また, 等級原点の誤差はおよそ 0.1 等級と推定された. 等級原点の値があまり小さい (浅い) 画像や, 誤差が大きい画像は続くモザイク画像の作成では用いないほうがよい.

\subsection{モザイク画像の作成}
\label{sec:reduction:mosaic}

モザイク画像の作成には \texttt{montage} というスクリプトを用いる. これは SDSS のカタログ等級と写っている星の明るさを比較して等級原点を計算し, 複数の画像で等級原点をマッチさせてから最終的に \texttt{SWarp} というソフトウエアでモザイク画像を作成するためのプログラムである.

\texttt{montage} のヘルプメッセージを以下に貼付する.

\begin{shell}
usage: montage [-h] [--pixel-scale scale] [--remove-background]
               [--background-bw size] [--background-fw size] [-f]
               ra dec image_w image_h fits [fits ...] output

generate a mosaic image using SWarp

positional arguments:
  ra                    right ascension of image center
  dec                   declination of image center
  image_w               image width in pixel
  image_h               image height in pixel
  fits                  input fits file
  output                output fits file

optional arguments:
  -h, --help            show this help message and exit
  --pixel-scale scale   pixel scale of output image in arcsec
  --combine method      combine method (average,median,min,max,chi-mean,sum)
  --remove-background   enable additional background subtraction
  --background-bw size  grid size in background estimation
  --background-fw size  filter size in background estimation
  --swarp-path path     path to the "swarp" executable command
  -f, --overwrite       overwrite existing files
\end{shell}

デフォルトでは Tomo-e Gozen のピクセルスケールにあわせて $1.2''\mathrm{pix^{-1}}$ で画像が合成される. Abell 2634 の中心に $1^{\circ}{\times}1^{\circ}$ の画像を作成したい場合には以下のようにコマンドを実行する.

\begin{shell}
$ ./montage 23:38:18.4 27:01:37 3000 3000 input0.fits input1.fits [...] output.fits
\end{shell}

問題なく実行されればカレントディレクトリに \texttt{output.fits}, \texttt{output.wt.fits} 2 つのファイルができているはずである. \texttt{output.fits} がモザイク合成された画像である.

デフォルトの設定では画像を足し合わせるときにカウントレベルを調整しない. 背景光を差し引いて足し合わせたい場合にはオプション \texttt{--remove-background} をつけて実行する. 足し合わせ方法を \texttt{average} から変更したい場合にはオプション \texttt{--combine} の引数に与える.


\clearpage
\subsection{役に立つウェブページ}
\label{sec:misc:webpage}
観測や解析で使用する可能性のあるウェブページを以下にまとめる. Tomo-e Gozen に関する重要なページは基本的にすべて Tomo-e Gozen Observer's Portal からリンクされている. ただし ``Tomo-e Gozen Field Checker'' 以外のページは木曽観測所のネットワーク以外からはアクセスできないので注意すること.

\setstretch{0.85}
\begin{itemize}[topsep=1.5em,leftmargin=2em,parsep=0em,itemsep=0.8em]
\item 基礎天文学観測「CMOS 撮像と画像解析」補助ページ\\
  \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/jisshu/}
  {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/jisshu/}
\item Tomo-e Gozen Observer's Portal\\
  \href{http://teru.kiso.ioa.s.u-tokyo.ac.jp/portal/}
  {\ttfamily http://teru.kiso.ioa.s.u-tokyo.ac.jp/portal/}\\[0.8em]
  Observer's Portal からは以下のページにアクセスできる.
  \begin{itemize}[topsep=0.5em,leftmargin=1.5em,parsep=0em,itemsep=0.4em]
  \item Weather Monitor\\
    \href{http://teru.kiso.ioa.s.u-tokyo.ac.jp/portal/weather/wmon.php}
    {\ttfamily http://teru.kiso.ioa.s.u-tokyo.ac.jp/portal/weather/wmon.php}
  \item All Sky Viewer\\
    \href{http://teru.kiso.ioa.s.u-tokyo.ac.jp/portal/weather/allsky.php}
    {\ttfamily http://teru.kiso.ioa.s.u-tokyo.ac.jp/portal/weather/allsky.php}
  \item Schmidt Status\\
    \href{http://teru.kiso.ioa.s.u-tokyo.ac.jp/tel_status/}
    {\ttfamily http://teru.kiso.ioa.s.u-tokyo.ac.jp/tel\_status/}
  \item Tomo-e Gozen Queue Monitor\\
    \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/queue/status}
    {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/queue/status}
  \item Tomo-e Gozen Exposure Log\\
    \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/queue/log}
    {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/queue/log}
  \item Tomo-e Gozen SkyMap\\
    \href{http://teru.kiso.ioa.s.u-tokyo.ac.jp/skymap/}
    {\ttfamily http://teru.kiso.ioa.s.u-tokyo.ac.jp/skymap/}
  \item Tomo-e Gozen Data Archive\\
    \href{http://tomoearv-master.kiso.ioa.s.u-tokyo.ac.jp/archive/}
    {\ttfamily http://tomoearv-master.kiso.ioa.s.u-tokyo.ac.jp/archive/}
  \item Tomo-e Gozen Quick Look Viewer\\[0.3em]
    ~~\makebox[4em][l]{$g'$ east}:
    \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/112}
    {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/112}\\
    ~~\makebox[4em][l]{$g'$ center}:
    \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/122}
    {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/122}\\
    ~~\makebox[4em][l]{$g'$ west}:
    \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/132}
    {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/132}\\
    ~~\makebox[4em][l]{$i'$ east}:
    \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/111}
    {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/111}\\
    ~~\makebox[4em][l]{$i'$ center}:
    \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/121}
    {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/121}\\
    ~~\makebox[4em][l]{$i'$ west}:
    \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/131}
    {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/131}\\
    ~~\makebox[4em][l]{$r'$ east}:
    \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/113}
    {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/113}\\
    ~~\makebox[4em][l]{$r'$ center}:
    \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/123}
    {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/123}\\
    ~~\makebox[4em][l]{$r'$ west}:
    \href{http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/133}
    {\ttfamily http://tomoe-gozen.kiso.ioa.s.u-tokyo.ac.jp/reduce/ql/133}
  \item Tomo-e Gozen Field Checker with DSS\\
    \href{http://www.ioa.s.u-tokyo.ac.jp/tomoe/aladin}
    {\ttfamily http://www.ioa.s.u-tokyo.ac.jp/tomoe/aladin}
  \end{itemize}
\end{itemize}


% \clearpage
% \subsection{おまけ (\texttt{imarith} と \texttt{montage} の中身)}
% \label{sec:reduction:misc}
% \inputrecipe{resource/imarith}{imarith}
% \clearpage
% \inputrecipe{resource/montage}{montage}

%%%%% Abstract %%%%%
\end{document}
